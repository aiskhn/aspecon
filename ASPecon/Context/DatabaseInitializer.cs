﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ASPecon.Context
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            var listEco = new List<Models.eco>()
            {
                new Models.eco() { Id = 1, Econame = "Nuss Achat", Count = 20},
                new Models.eco() { Id = 2, Econame = "Pauschal", Count = 20},
                new Models.eco() { Id = 3, Econame = "Wenge", Count = 20}
            };

            listEco.ForEach(x => context.ecos.Add(x));
            context.SaveChanges();
        }
    }
}