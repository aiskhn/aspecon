﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASPecon.Context;
using ASPecon.Models;

namespace ASPecon.Controllers
{
    public class EconController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Econ
        public ActionResult Index()
        {
            return View(db.ecos.ToList());
        }

        // GET: Econ/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eco eco = db.ecos.Find(id);
            if (eco == null)
            {
                return HttpNotFound();
            }
            return View(eco);
        }

        // GET: Econ/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Econ/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Econame,Count")] eco eco)
        {
            if (ModelState.IsValid)
            {
                db.ecos.Add(eco);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eco);
        }

        // GET: Econ/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eco eco = db.ecos.Find(id);
            if (eco == null)
            {
                return HttpNotFound();
            }
            return View(eco);
        }

        // POST: Econ/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Econame,Count")] eco eco)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eco).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eco);
        }

        // GET: Econ/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eco eco = db.ecos.Find(id);
            if (eco == null)
            {
                return HttpNotFound();
            }
            return View(eco);
        }

        // POST: Econ/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eco eco = db.ecos.Find(id);
            db.ecos.Remove(eco);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
