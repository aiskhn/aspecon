﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPecon.Models
{
    public class eco
    {
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Econame { get; set; }
        public int Count { get; set; }
    }
}